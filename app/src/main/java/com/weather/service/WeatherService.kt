package com.weather.service

import com.weather.model.AreaModel
import com.weather.model.WeatherModel
import io.reactivex.Observable
import io.reactivex.Single

import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {


    @GET("geo/1.0/direct")
    fun getCitiesGeoCoordinates(@Query("q") areaName:String, @Query("appid") appId:String = "24e1bcd1a967fb162931527d46c953dd"): Observable<List<AreaModel>?>

    @GET("data/2.5/weather")
    fun getWeatherInfoOfArea(@Query("lat") lat:Double, @Query("lon") lon:Double, @Query("appid") appId:String = "24e1bcd1a967fb162931527d46c953dd"): Observable<WeatherModel>

}