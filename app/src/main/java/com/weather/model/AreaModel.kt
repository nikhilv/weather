package com.weather.model

data class AreaModel (
    val name: String,
    val lat: Double,
    val lon: Double,
    val country: String,
    val state: String
)