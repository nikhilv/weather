package com.weather.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.weather.di.modules.NetworkModule
import com.weather.model.AreaModel
import com.weather.model.WeatherModel
import com.weather.repository.ApiRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class WeatherViewModel @Inject constructor(app: Application, private val repoRepository: ApiRepository): AndroidViewModel(app) {

    private var disposable: CompositeDisposable? = null
    private val areaModels = MutableLiveData<List<AreaModel>>()
    private val repoLoadError = MutableLiveData<Boolean>()
    private val loading = MutableLiveData<Boolean>()
    var weatherModel = MutableStateFlow<WeatherModel?>(null)

    var weatherStateFlow = weatherModel.asStateFlow()


    fun getError(): LiveData<Boolean> {
        return repoLoadError
    }

    fun getLoading(): LiveData<Boolean> {
        return loading
    }

    fun fetchAreas(areaName: String) {
        loading.value = true
        disposable!!.add(
            repoRepository.getAreaDetails(areaName)?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeWith(object : DisposableObserver<List<AreaModel?>?>() {

                    override fun onError(e: Throwable) {
                        repoLoadError.value = true
                        loading.value = false
                    }

                    override fun onNext(value: List<AreaModel?>?) {
                        repoLoadError.value = false
                        areaModels.value = value as List<AreaModel>
                        if(value.isNotEmpty()){
                            fetchWeather(value[0].lat, value[0].lon)
                        }
                    }

                    override fun onComplete() {
                        loading.value = false
                    }
                })
        )
    }

    fun fetchWeather(lat: Double, lon: Double) {
        loading.value = true
        disposable!!.add(
            repoRepository.getWeatherDetails(lat, lon)?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeWith(object : DisposableObserver<WeatherModel>() {

                    override fun onError(e: Throwable) {
                        repoLoadError.value = true
                        loading.value = false
                    }

                    override fun onNext(value: WeatherModel) {
                        repoLoadError.value = false
                        weatherModel.value = value
                    }

                    override fun onComplete() {
                        loading.value = false
                    }
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        if (disposable != null) {
            disposable!!.clear()
            disposable = null
        }
    }

    init {
        disposable = CompositeDisposable()
    }

}