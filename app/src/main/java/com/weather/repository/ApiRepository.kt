package com.weather.repository

import com.weather.model.AreaModel
import com.weather.model.WeatherModel
import com.weather.service.WeatherService
import io.reactivex.Observable

import javax.inject.Inject

class ApiRepository @Inject constructor(private val apiServices: WeatherService) {

    fun getAreaDetails(areaName : String): Observable<List<AreaModel>?> {
        return apiServices.getCitiesGeoCoordinates(areaName = areaName)
    }

    fun getWeatherDetails(lat: Double, lon: Double): Observable<WeatherModel> {
        return apiServices.getWeatherInfoOfArea(lat = lat, lon = lon)
    }
}