package com.weather.view

import android.os.Bundle
import android.view.Window
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.lifecycleScope
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.weather.R
import com.weather.model.WeatherModel
import com.weather.ui.theme.MyTheme
import com.weather.viewmodel.ViewModelFactory
import com.weather.viewmodel.WeatherViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class WeatherActivity : DaggerAppCompatActivity() {


    @Inject
    lateinit var factory: ViewModelFactory

    private lateinit var viewModel: WeatherViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContent {
            MyTheme {
                MyApp(viewModel)
            }
        }
        viewModel = ViewModelProviders.of(this, factory)[WeatherViewModel::class.java]
    }

    @Composable
    fun MyApp(model : WeatherViewModel) {
        WeatherView(model)
    }


}