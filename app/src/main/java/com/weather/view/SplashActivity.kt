package com.weather.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Window
import com.weather.R
import com.weather.application.MyApplication
import com.weather.di.AppComponent
import com.weather.di.DaggerAppComponent
import dagger.android.support.DaggerAppCompatActivity

class SplashActivity : DaggerAppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_splash)
        val r = Runnable {
            startActivity(Intent(this, WeatherActivity::class.java))
            finish()
        }
        val h = Handler()

        h.postDelayed(r, 10)
    }
}