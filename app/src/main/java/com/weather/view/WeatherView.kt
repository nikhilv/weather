package com.weather.view

import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.weather.R
import com.weather.extension.convertKtoC
import com.weather.extension.toTimeDateString
import com.weather.model.WeatherModel
import com.weather.ui.theme.btnColor
import com.weather.viewmodel.WeatherViewModel
import com.weather.ui.theme.lightBlue
import com.weather.ui.theme.oceanBlue

@Composable
fun WeatherView(vm: WeatherViewModel) {
   /* val color1 by backgroundColorState(curWeather.background.first)
    val color2 by backgroundColorState(curWeather.background.second)
    val color3 by backgroundColorState(curWeather.background.third)*/

    val viewModel by remember { mutableStateOf(vm) }
    Column(
        modifier = Modifier
            .fillMaxSize()
    ){
        TxtField(viewModel = viewModel)
        WeatherDisplay(viewModel)
    }
}

@Composable
fun WeatherDisplay(viewModel: WeatherViewModel) {
    viewModel.weatherStateFlow.collectAsState().value.let { data ->
            Column(
                modifier = Modifier
                    .width(800.dp)
                    .fillMaxHeight()
                    .paint(painterResource(id = R.drawable.bg))
            ) {
                if (data != null) {
                    WeatherInfoPager(
                        Modifier
                            .weight(1f)
                            .align(Alignment.CenterHorizontally)
                            .semantics(true) {
                                contentDescription = ""
                            },
                        data
                    )
                }
            }
        }

}

/**
 * Pager showing weather info
 */
@Composable
fun WeatherInfoPager(
    modifier: Modifier = Modifier,
    model: WeatherModel
) {
    Column(modifier = Modifier.fillMaxSize()) {
        Spacer(modifier = Modifier.height(40.dp))
        Text(
            toTimeDateString(model.dt),
            style = TextStyle(
                fontSize = 18.sp,
                fontWeight = FontWeight.SemiBold,
                textAlign = TextAlign.Center,
                color = Color.White
            ),
            modifier = Modifier.align(Alignment.CenterHorizontally)
        )
        Spacer(modifier = Modifier.height(4.dp))
        Text(
            model.weather[0].description,
            style = TextStyle(
                fontSize = 18.sp,
                fontWeight = FontWeight.SemiBold,
                color = Color.White
            ),
            modifier = Modifier.align(Alignment.CenterHorizontally)
        )
        WeatherIcon(
            modifier = Modifier.align(Alignment.CenterHorizontally),
            model.weather[0].icon
        )

        Spacer(modifier = Modifier.height(2.dp))

        Row(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .height(100.dp)
        ) {
            Column(modifier = Modifier.align(Alignment.CenterVertically)) {
                Text(
                    "${convertKtoC(model.main.temp_max)} ⬆ ",
                    style = TextStyle(
                        fontSize = 19.sp,
                        fontWeight = FontWeight.SemiBold,
                        color = Color.White
                    ),
                    modifier = Modifier.align(Alignment.End)
                )
                Spacer(modifier = Modifier.height(3.dp))
                Text(
                    "${convertKtoC(model.main.temp_min)} ⬇ ",
                    style = TextStyle(
                        fontSize = 19.sp,
                        fontWeight = FontWeight.SemiBold,
                        color = Color.White
                    ),
                    modifier = Modifier.align(Alignment.End)
                )
            }

            val animateTween by rememberInfiniteTransition().animateFloat(
                initialValue = -1f,
                targetValue = 1f,
                animationSpec = infiniteRepeatable(
                    tween(2000, easing = LinearEasing),
                    RepeatMode.Reverse
                )
            )

            Text(
                convertKtoC(model.main.temp),
                style = TextStyle(
                    fontSize = 70.sp,
                    fontWeight = FontWeight.SemiBold,
                    color = Color.White
                ),
                letterSpacing = 0.sp,
                modifier = Modifier
                    .offset(0.dp, (-5).dp * animateTween)
                    .padding(10.dp, 0.dp)
            )
            Text(
                "℃",
                style = TextStyle(
                    fontSize = 30.sp,
                    fontWeight = FontWeight.SemiBold,
                    color = Color.White
                ),
                modifier = Modifier.padding(top = 10.dp)
            )
        }
    }
}

@Composable
fun WeatherIcon(modifier: Modifier, weatherIcon: String) {
    Column(
        modifier = modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AsyncImage(
            model = "https://openweathermap.org/img/wn/$weatherIcon.png",
            contentDescription = "gfg image",
            modifier = Modifier.size(120.dp)
        )
    }
}

@Composable
fun TxtField(viewModel: WeatherViewModel) {
    val inputvalue = remember { mutableStateOf(TextFieldValue()) }
    Column( modifier = Modifier
        .fillMaxWidth()
        .background(color = oceanBlue)) {
        Spacer(modifier = Modifier.height(12.dp))
        Row(modifier = Modifier
            .fillMaxWidth()
            .background(color = oceanBlue))
        {
            TextField(value = inputvalue.value ,
                placeholder = { Text("Search with city name") },
                onValueChange = { inputvalue.value = it },
                modifier = Modifier
                    .padding(10.dp, 0.dp)
                    .background(color = Color.White, shape = RoundedCornerShape(10.dp))
            )
            Button(onClick = { if(inputvalue.value.text.isNotEmpty()) viewModel.fetchAreas(inputvalue.value.text) },
                modifier = Modifier.padding(0.dp, 5.dp),
                shape = RoundedCornerShape(15.dp),
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Transparent)
            ) {
                Text(text = "Search", color = Color.White)
            }
        }
        Spacer(modifier = Modifier.height(12.dp))
    }
}