package com.weather.extension

import com.weather.model.Main
import com.weather.model.WeatherModel
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

fun toTimeDateString(dateTime: Long): String {
    val dateTime = Date(dateTime*1000)
    val format = SimpleDateFormat("EE, dd MMM yyyy", Locale.US)
    return format.format(dateTime)
}

fun convertKtoC(value: Double): String {
    return DecimalFormat("0.00").format(value - 273.15f)
}
