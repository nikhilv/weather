package com.weather.di.modules

import com.weather.view.SplashActivity
import com.weather.view.WeatherActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBuilderModule {

    @ContributesAndroidInjector
    fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    fun contributeWeatherActivity(): WeatherActivity
}