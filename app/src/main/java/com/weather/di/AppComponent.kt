package com.weather.di

import android.app.Application
import com.weather.application.MyApplication
import com.weather.di.modules.ActivityBuilderModule
import com.weather.di.modules.NetworkModule
import com.weather.di.modules.ViewModelModule
import com.weather.view.SplashActivity
import com.weather.view.WeatherActivity
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class, NetworkModule::class, ViewModelModule::class, ActivityBuilderModule::class]
)
interface AppComponent: AndroidInjector<MyApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(app: MyApplication)

    fun inject(homeActivity: SplashActivity)

    fun inject(homeActivity: WeatherActivity)
}